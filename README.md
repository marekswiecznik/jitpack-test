# JitPack Test Android Library README

Add in root build.gradle:
```
allprojects {
 repositories {
    jcenter()
    maven { url "https://jitpack.io" }
 }
}
```

and in your module (eg. app):
```dependencies {
    compile 'org.bitbucket.marekswiecznik:jitpack-test:1.1'
}
```


